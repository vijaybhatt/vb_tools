#!/usr/bin/perl 
  
# Modules used 
use strict; 
use warnings; 
use Text::ParseWords;


my $file_name = $ARGV[0];
my $date_str = $ARGV[1];
my $out_file = $ARGV[2];

# Print function  
print("Parse Traefik Access Log\n"); 

open(my $fh, "<", $file_name) or die "Can't open < file: $!";

my %uri_count;
my %uri_responsetime_array;
my %uri_response_total;

# Process every line in input.txt
while (my $line = <$fh>) {

	my @array, my @words, my $endpoint, my $response_time;

	if ($line =~ m/$date_str/) {

		#@words = split ' ', $line;
		@words = parse_line(" ", 0, $line);
		$endpoint = $words[5];
		$response_time = $words[13];

		$response_time = substr($response_time, 0, -3);

		if(exists($uri_count{$endpoint})) {
			$uri_count{$endpoint}++;
			push(@{$uri_responsetime_array{$endpoint}}, $response_time);
		} else {
			$uri_count{$endpoint} = 1;
			@array = ($response_time);
			$uri_responsetime_array{$endpoint} = \@array;
		}
	}

}

open(my $fh_out, ">", $out_file) or die "Can't open > file: $!";
print("Writing to outfile\n");
foreach my $key (keys(%uri_count)) {
	print $fh_out $key . "," . $uri_count{$key} . "," . average(@{$uri_responsetime_array{$key}}) . "," . percentile90(@{$uri_responsetime_array{$key}}) . "\n";

	#print $fh_out "response times :";
	#foreach my $response_time (@{$uri_responsetime_array{$key}}) {
	#	print $fh_out " " . $response_time;
	#}
	#print $fh_out "\n";
}

sub average {

   my @list = @_;

   my $n = scalar(@list);
   my $sum = 0;

   foreach my $item (@list) {
      $sum += $item;
   }
   my $average = $sum / $n;
   return $average;
}

sub percentile90 {
	my @values = @_;
	# Sort
	@values = sort {$a <=> $b} @values;

	# derive 90th percentile
	return $values[sprintf("%.0f",(0.90*($#values)))];
}
